# Auto Test for Project 2

This is a setup for auto test of Project 2 in R. 

## Step 1: Fork the repo and make the fork private
* After forking, go to https://gitlab.com/<your-name\>/<project name\>/edit, expand "Visibility, project features, permissions" and then set the forked project as private.
## Step 2: Finish your own version of mymain.R and upload your mymain.R
* Do NOT add or change any other file.
* Once mymain.R is uploaded, the gitlab runner will automatically start and run the script.
* When the job is finished, you will see a green check / red cross mark on the project page. The green check means the run succeeds and the red means failure.
* If the run succeeds, skip the rest in step 2 and then go to step 3.
* You will receive an email notification (if you are using default gitlab email setting). 
* If you want to see environment output, go to https://gitlab.com/<your name\>/<project name\>/jobs/ and click on the status 'Failed'.
## Step 3: Download the results
* Hang your mouse over the download sign to see the download menu. Click on "test" under "Download artifacts" to download the results.
* Unzip the downloaded "artifacts.zip" and check the log.txt file to see the results.

